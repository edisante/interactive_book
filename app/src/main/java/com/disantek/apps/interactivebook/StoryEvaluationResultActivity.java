package com.disantek.apps.interactivebook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import data.StoryAnswer;

public class StoryEvaluationResultActivity extends AppCompatActivity {

    private List<StoryAnswer> answerList;
    private boolean isPuzzleCorrect;

    private TextView tvResultUserName;
    private TextView tvResultNumberCorrectQuestions;
    private TextView tvResultCorrectAnswerRate;
    private TextView tvResultPuzzleResult;
    private ImageView ivResultPuzzleCorrectOrder;
    private int storyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_evaluation_result);

        answerList = new ArrayList<>();

        storyId = getIntent().getIntExtra("storyId", -1);
        Bundle bundle = getIntent().getExtras();
        answerList = bundle.getParcelableArrayList("evaluationResult");
        this.isPuzzleCorrect = getIntent().getBooleanExtra("isPuzzleCorrect", false);

        tvResultUserName = (TextView) findViewById(R.id.tvResultUserName);
        tvResultNumberCorrectQuestions = (TextView) findViewById(R.id.tvResultNumberCorrectQuestions);
        tvResultCorrectAnswerRate = (TextView) findViewById(R.id.tvResultCorrectAnswerRate);
        tvResultPuzzleResult = (TextView) findViewById(R.id.tvResultPuzzleResult);
        ivResultPuzzleCorrectOrder = (ImageView) findViewById(R.id.ivResultPuzzleCorrectOrder);

        processAndShowResults();

        Button btnEnd = (Button) findViewById(R.id.btnEndResults);
        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    private void processAndShowResults() {

        tvResultUserName.setText(SharedPreference.getUserName(this));

        int correctAnswerNumber = 0;

        if (answerList != null && answerList.size() > 0){

            for (int i = 0; i < answerList.size(); i++) {
                if (answerList.get(i).isCorrectAnswer() == 1){
                    correctAnswerNumber++;
                }
            }

            float correctAnswerRate = (float) (((float)correctAnswerNumber/(float)answerList.size())*100);

            DecimalFormat df = new DecimalFormat("#.##");
            df.setRoundingMode(RoundingMode.CEILING);

            tvResultNumberCorrectQuestions.setText(correctAnswerNumber + "");
            tvResultCorrectAnswerRate.setText(df.format(correctAnswerRate) + "%");
        }

        if (isPuzzleCorrect){
            tvResultPuzzleResult.setText(R.string.correct);
            tvResultPuzzleResult.setTextColor(getResources().getColor(R.color.answer_correct));
        }
        else {
            tvResultPuzzleResult.setText(R.string.incorrect);
            tvResultPuzzleResult.setTextColor(getResources().getColor(R.color.answer_incorrect));
        }

        if (storyId == 1){
            this.ivResultPuzzleCorrectOrder.setImageResource(R.drawable.puzzle_1_correct);
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }
}
