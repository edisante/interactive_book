package com.disantek.apps.interactivebook;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

import data.StoryAnswer;
import data.StoryQuestion;

/**
 * Created by edisante@gmail.com on 26/10/2017.
 */

public class StoryEvaluationViewPagerAdapter extends PagerAdapter implements AdapterView.OnItemClickListener{

    private Context mContext;
    private List<StoryQuestion> questionList;
    private CustomViewPager viewPager;
    private StoryEvaluationActivity storyEvaluationActivity;

    private ArrayList<StoryAnswer> evaluationResult;

    public StoryEvaluationViewPagerAdapter(Context mContext, List<StoryQuestion> questionList, CustomViewPager viewPager, StoryEvaluationActivity storyEvaluationActivity) {
        this.mContext = mContext;
        this.questionList = questionList;
        this.viewPager = viewPager;
        evaluationResult = new ArrayList<>();
        this.storyEvaluationActivity = storyEvaluationActivity;
    }

    @Override
    public int getCount() {
        return questionList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        //int resId = mContext.getResources().getIdentifier(questionList.get(position).getQuestionLayoutResource(), "layout", mContext.getPackageName());
        View itemView = null;
        itemView = LayoutInflater.from(mContext).inflate(R.layout.layout_question_1, container, false);

        //if (position == 0){
            //ImageButton btnQ1A1 = itemView.findViewById(R.id.ibAnswer1Question1);

        TextView tvQuestion = (TextView)itemView.findViewById(R.id.tvQuestion);
        tvQuestion.setText(questionList.get(position).getQuestion());

        ExpandableHeightListView listView = (ExpandableHeightListView) itemView.findViewById(R.id.lvAnswers);
        EvaluationAdapter adapter = new EvaluationAdapter(mContext, questionList.get(position).getAnswerList());

        listView.setAdapter(adapter);
        listView.setExpanded(true);
        listView.setTag(questionList.get(position).getId());

        listView.setOnItemClickListener(this);
        //}

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //Log.i(mContext.getClass().getName(), "onItemClick: click!!");
        List<StoryAnswer> answers = questionList.get((int)adapterView.getTag()-1).getAnswerList();
        evaluationResult.add(answers.get(i));

        if (viewPager.getCurrentItem() == questionList.size()-1){
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("evaluationResult", evaluationResult);
            storyEvaluationActivity.callActivityStoryPuzzle(bundle);
        }

        if (viewPager.getCurrentItem() < questionList.size()-1){
            viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
        }
    }
}
