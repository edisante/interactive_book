package com.disantek.apps.interactivebook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import data.DBHelper;
import data.Story;

public class StoryListActivity extends AppCompatActivity {

    private ListView lvStories;
    private StoryListAdapter adapter;
    private List<Story> storyList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_list);

        lvStories = (ListView)findViewById(R.id.lvStoryList);
        storyList = new ArrayList<>();

        new LoadStoryList().execute();

        lvStories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (storyList != null && storyList.size() > 0){
                    Intent intent = new Intent(StoryListActivity.this, PlayStoryActivity.class);
                    intent.putExtra("storyId", storyList.get(i).getId());
                    intent.putExtra("storyTitle", storyList.get(i).getTitle());
                    startActivity(intent);
                }
            }
        });
    }

    private class LoadStoryList extends AsyncTask<Void, Void, List<Story>>{

        private ProgressDialog progressDialog;
        private DBHelper dbHelper = new DBHelper(getBaseContext());

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(StoryListActivity.this, getString(R.string.loading), null, true);
        }

        @Override
        protected List<Story> doInBackground(Void... voids) {
            return dbHelper.getStories();
        }

        @Override
        protected void onPostExecute(List<Story> stories) {
            storyList.clear();
            adapter = null;

            if (stories != null && stories.size() > 0){
                storyList = stories;

                adapter = new StoryListAdapter(getBaseContext(), storyList);
                lvStories.setAdapter(adapter);
            }
            else {
                Toast.makeText(getBaseContext(), getString(R.string.not_stories_available), Toast.LENGTH_LONG).show();
            }

            //if (progressDialog != null)
                progressDialog.dismiss();
        }
    }
}
