package com.disantek.apps.interactivebook;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.disantek.apps.interactivebook.curl.CurlView;

import java.util.List;

import data.DBHelper;
import data.Page;
import data.Story;

public class PlayStoryActivity extends AppCompatActivity {

    protected View view;
    private Button btnNext, btnBack;
    private ViewPager viewPager;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private TextView[] dots;
    private ViewPagerAdapter mAdapter;
    private TextView tvPageNumber;

    private int currentPage = 0;

    private CurlView mCurlView;

    private Story story;

    private int[] storyPageImageText = {
            /*R.drawable.story_1_text_1,
            R.drawable.story_1_text_2,
            R.drawable.story_1_text_3,
            R.drawable.story_1_text_4,
            R.drawable.story_1_text_5,
            R.drawable.story_1_text_6,
            R.drawable.story_1_text_7,
            R.drawable.story_1_text_8*/
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_play_story);

        story = new Story();
        story.setId(getIntent().getIntExtra("storyId", -1));
        story.setTitle(getIntent().getStringExtra("storyTitle"));

        getSupportActionBar().setTitle(story.getTitle());

        if (getLastCustomNonConfigurationInstance() != null) {
            currentPage = (Integer) getLastCustomNonConfigurationInstance();
        }

        pager_indicator = (LinearLayout)findViewById(R.id.layoutDots);
        btnNext = (Button)findViewById(R.id.btn_next);
        btnNext.setText(R.string.finalize);
        btnBack = (Button)findViewById(R.id.btn_skip);
        btnBack.setVisibility(View.INVISIBLE);
        tvPageNumber = (TextView)findViewById(R.id.tvPageNumber);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnNextOnClick(view);
            }
        });

        new LoadStoryPages().execute(story.getId());

    }

    private void btnNextOnClick(View view) {

        finish();
        Intent intent = new Intent(this, StoryEvaluationActivity.class);
        intent.putExtra("storyId", story.getId());
        startActivity(intent);
    }

    private class LoadStoryPages extends AsyncTask<Integer, Void, List<Page>>{

        ProgressDialog progressDialog;
        private DBHelper dbHelper = new DBHelper(getBaseContext());

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(PlayStoryActivity.this, getString(R.string.loading), null, true);
        }

        @Override
        protected List<Page> doInBackground(Integer... storyId) {
            return dbHelper.getStoryPageList(storyId[0]);
        }

        @Override
        protected void onPostExecute(List<Page> pages) {

            progressDialog.dismiss();

            if (pages == null || pages.size() < 1){
                Toast.makeText(getBaseContext(), "El cuento no tiene páginas asociadas", Toast.LENGTH_LONG).show();
                return;
            }

            int [] pageResources = new int[pages.size()];

            for (int i = 0; i < pages.size(); i++) {

                String resName = pages.get(i).getResourceName();

                int resId = getResources().getIdentifier(resName, "drawable", getPackageName());
                if (resId > 1){
                    pageResources[i] = resId;
                }
            }

            storyPageImageText = pageResources;

            mCurlView = (CurlView) findViewById(R.id.view_pager);
            mCurlView.setBitmapProvider(new BitmapProvider(storyPageImageText));
            mCurlView.setSizeChangedObserver(new SizeChangedObserver());
            mCurlView.setCurrentIndex(currentPage);
            mCurlView.setBackgroundColor(0xFF202830);

            // This is something somewhat experimental. Before uncommenting next
            // line, please see method comments in CurlView.
            mCurlView.setEnableTouchPressure(true);

            // CAGS: This is to allow 2 pages landscape mode, set to false for legacy mode
            mCurlView.set2PagesLandscape(true);

            //if (progressDialog != null)

        }
    }

    /**
     * Bitmap provider.
     */
    private class BitmapProvider implements CurlView.BitmapProvider {

        private int[] mBitmapIds;

        public BitmapProvider(int[] mBitmapIds) {
            this.mBitmapIds = mBitmapIds;
        }

        @Override
        public Bitmap getBitmap(int width, int height, int index) {
            Bitmap b = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            b.eraseColor(0xFFFFFFFF);
            Canvas c = new Canvas(b);
            Drawable d = getResources().getDrawable(mBitmapIds[index]);

            int margin = 7;
            int border = 0;
            Rect r = new Rect(margin, margin, width - margin, height - margin);

            int imageWidth = r.width() - (border * 2);
            int imageHeight = imageWidth * d.getIntrinsicHeight()
                    / d.getIntrinsicWidth();
            if (imageHeight > r.height() - (border * 2)) {
                imageHeight = r.height() - (border * 2);
                imageWidth = imageHeight * d.getIntrinsicWidth()
                        / d.getIntrinsicHeight();
            }

            r.left += ((r.width() - imageWidth) / 2) - border;
            r.right = r.left + imageWidth + border + border;
            r.top += 150;//((r.height() - imageHeight) / 2) - border;
            r.bottom = r.top + imageHeight + border + border;

            Paint p = new Paint();
            p.setColor(0xFFC0C0C0);
            c.drawRect(r, p);
            r.left += border;
            r.right -= border;
            r.top += border;
            r.bottom -= border;

            d.setBounds(r);
            d.draw(c);
            return b;
        }

        @Override
        public int getBitmapCount() {
            return mBitmapIds.length;
        }
    }

    /**
     * CurlView size changed observer.
     */
    private class SizeChangedObserver implements CurlView.SizeChangedObserver {
        @Override
        public void onSizeChanged(int w, int h) {
            if (w > h) {
                mCurlView.setViewMode(CurlView.SHOW_TWO_PAGES);
                mCurlView.setMargins(.0f, .0f, .0f, .0f);
            } else {
                mCurlView.setViewMode(CurlView.SHOW_ONE_PAGE);
                mCurlView.setMargins(.0f, .0f, .0f, .0f);
            }
        }
    }
}
