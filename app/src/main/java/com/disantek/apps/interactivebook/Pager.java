package com.disantek.apps.interactivebook;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by edisante@gmail.com on 09/08/17.
 */

public class Pager extends FragmentStatePagerAdapter{

    private int tabCount;

    public Pager(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:{
                return new TabReading();
            }
            case 1:{
                return new TabGames();
            }
            default:{
                return null;
            }
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}
