package com.disantek.apps.interactivebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import data.StoryAnswer;

/**
 * Created by edisante@gmail.com on 27/10/2017.
 */

public class EvaluationAdapter extends BaseAdapter{

    private Context context;
    List<StoryAnswer> answerList;

    private static class ViewHolder {
        private ImageView ivAnswer;
    }

    public EvaluationAdapter(Context context, List<StoryAnswer> answerList) {
        this.context = context;
        this.answerList = answerList;
    }

    @Override
    public int getCount() {
        return answerList.size();
    }

    @Override
    public Object getItem(int i) {
        return answerList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder = new ViewHolder();

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.lv_answer_item, null);
        }

        viewHolder.ivAnswer = (ImageView)view.findViewById(R.id.ivAnswer);
        int resId = context.getResources().getIdentifier(answerList.get(i).getAnswerIcon(), "drawable", context.getPackageName());
        if (resId > 1){
            viewHolder.ivAnswer.setImageResource(resId);
        }

        view.setTag(i);

        return view;
    }
}
