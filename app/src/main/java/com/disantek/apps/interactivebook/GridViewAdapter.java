package com.disantek.apps.interactivebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.askerov.dynamicgrid.BaseDynamicGridAdapter;

import java.util.List;

import data.StoryAnswer;
import data.StoryPuzzlePiece;

/**
 * Created by edisante@gmail.com on 29/10/2017.
 */

public class GridViewAdapter extends BaseDynamicGridAdapter {

    private List<StoryPuzzlePiece> puzzlePieceList;

    public GridViewAdapter(Context context, List<?> items, int columnCount) {
        super(context, items, columnCount);
        this.puzzlePieceList = (List<StoryPuzzlePiece>) items;

    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.grid_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }
        holder.build(getItem(i));
        return view;
    }

    private class ViewHolder {
        private ImageView ivPuzzleImg;

        private ViewHolder(View view){
            this.ivPuzzleImg = (ImageView) view.findViewById(R.id.ivPuzzleImg);
        }

        private void build(Object object){
            StoryPuzzlePiece puzzlePiece = (StoryPuzzlePiece) object;
            int resId = getContext().getResources().getIdentifier(puzzlePiece.getImgResource(), "drawable", getContext().getPackageName());
            if (resId > 1){
                ivPuzzleImg.setImageResource(resId);
            }
            else {
                ivPuzzleImg.setImageResource(R.mipmap.ic_launcher_logo);
            }
        }
    }
}
