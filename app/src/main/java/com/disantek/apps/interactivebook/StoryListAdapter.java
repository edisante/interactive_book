package com.disantek.apps.interactivebook;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import data.Story;

/**
 * Created by edisante@gmail.com on 10/18/2017.
 */

class StoryListAdapter extends BaseAdapter{

    private Context context;
    private List<Story> storyList;

    private static class ViewHolder {
        private TextView tvStoryTitle;
        private ImageView ivStoryIcon;
    }

    public StoryListAdapter(Context context, List<Story> storyList) {
        this.context = context;
        this.storyList = storyList;
    }

    @Override
    public int getCount() {
        return storyList.size();
    }

    @Override
    public Object getItem(int i) {
        return storyList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder = new ViewHolder();

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.story_list_item, null);
        }

        viewHolder.tvStoryTitle = (TextView)view.findViewById(R.id.tvStoryTitle);
        viewHolder.ivStoryIcon = (ImageView)view.findViewById(R.id.ivStoryIcon);

        viewHolder.tvStoryTitle.setText(storyList.get(i).getTitle());
        int resId = context.getResources().getIdentifier(
                storyList.get(i).getIconResource(),
                "drawable",
                context.getPackageName());

        if (resId > 1){
            viewHolder.ivStoryIcon.setImageResource(resId);
        }

        view.setTag(i);

        if (i % 2 == 1)
            view.setBackgroundColor(Color.parseColor("#eaf8ff"));
        else
            view.setBackgroundColor(Color.WHITE);

        return view;
    }
}
