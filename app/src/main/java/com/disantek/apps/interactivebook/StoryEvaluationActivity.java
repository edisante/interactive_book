package com.disantek.apps.interactivebook;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import data.DBHelper;
import data.StoryQuestion;

public class StoryEvaluationActivity extends AppCompatActivity {

    private CustomViewPager viewPager;
    private StoryEvaluationViewPagerAdapter mAdapter;
    int currentPage = 0;
    private int storyId;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_evaluation);

        storyId = getIntent().getIntExtra("storyId", -1);

        loadQuestionByStory();

        viewPager = (CustomViewPager) findViewById(R.id.viewPager);

    }

    @SuppressLint("HandlerLeak")
    private void loadQuestionByStory() {

        if (storyId < 1){
            Log.e(getBaseContext().getClass().getName(), "loadQuestionByStory: storyId is null");
            return;
        }

        final ProgressDialog progressDialog = ProgressDialog.show(StoryEvaluationActivity.this, getString(R.string.loading), null, true);

        handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {

                switch (msg.arg1){
                    case 1:{
                        List<StoryQuestion> questionList = new ArrayList<>();
                        questionList = (List<StoryQuestion>)msg.obj;
                        mAdapter = new StoryEvaluationViewPagerAdapter(StoryEvaluationActivity.this, questionList, viewPager, StoryEvaluationActivity.this);
                        viewPager.setAdapter(mAdapter);
                        viewPager.setCurrentItem(currentPage);
                        viewPager.setSwipeable(false);
                        break;
                    }
                    case -1:{
                        Toast.makeText(getBaseContext(), "Error cargando preguntas.", Toast.LENGTH_LONG).show();
                        break;
                    }
                }

                if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        };

        new Thread(new Runnable() {
            @Override
            public void run() {
                DBHelper mDbHelper = new DBHelper(getBaseContext());

                List<StoryQuestion> questionList = new ArrayList<>();

                questionList = mDbHelper.getQuestionsByStory(storyId);

                Message message = new Message();

                if (questionList != null){
                    message.arg1 = 1;
                    message.obj = questionList;
                }
                else {
                    message.arg1 = -1;
                }
                handler.sendMessage(message);
            }
        }).start();
    }

    public void callActivityStoryPuzzle(Bundle bundle){
        //Log.i(getBaseContext().getClass().getName(), "callActivityStoryPuzzle: funciona!!");

        Intent intent = new Intent(this, StoryPuzzleActivity.class);
        intent.putExtras(bundle);
        intent.putExtra("storyId", storyId);
        startActivity(intent);
    }

}
