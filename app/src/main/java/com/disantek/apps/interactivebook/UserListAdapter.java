package com.disantek.apps.interactivebook;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import data.User;

/**
 * Created by edisante@gmail.com on 10/17/2017.
 */

class UserListAdapter extends BaseAdapter{

    private Context context;
    private List<User> userList;

    private static class ViewHolder {
        private TextView tvUserName;
        private ImageView ivAvatar;
    }

    public UserListAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return userList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder viewHolder = new ViewHolder();

        if (view == null){
            LayoutInflater layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.lv_user_item, null);
        }

        viewHolder.tvUserName = (TextView)view.findViewById(R.id.tvUserItemName);
        viewHolder.tvUserName.setText(userList.get(i).getUserName());

        viewHolder.ivAvatar = (ImageView)view.findViewById(R.id.ivUserItemAvatar);
        viewHolder.ivAvatar.setImageResource(R.drawable.ic_action_person);

        view.setTag(i);

        if (i % 2 == 1)
            view.setBackgroundColor(Color.parseColor("#eaf8ff"));
        else
            view.setBackgroundColor(Color.WHITE);

        return view;
    }
}
