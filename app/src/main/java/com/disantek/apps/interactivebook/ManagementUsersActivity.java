package com.disantek.apps.interactivebook;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import data.DBHelper;
import data.User;

public class ManagementUsersActivity extends AppCompatActivity {

    private static final int RESULT_CODE_MANAGEMENT_USER = 100;
    private DBHelper dbHelper;

    private ListView lvUsers;
    private UserListAdapter adapter;
    private List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_management_users);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnNewUser = (Button)findViewById(R.id.btnNewUser);

        dbHelper = new DBHelper(this);
        userList = new ArrayList<>();

        lvUsers = (ListView)findViewById(R.id.lvUsers);

        reloadUserList();

        btnNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog dialog = dialogNewUserForm();
                dialog.show();
            }
        });

        lvUsers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (userList.size() > 0){
                    setUserSesion(i);
                }
            }
        });
    }

    private void setUserSesion(int position) {

        SharedPreference.clearSession(this);

        SharedPreference.setUserId(this, userList.get(position).getId());
        SharedPreference.setUserName(this, userList.get(position).getUserName());
        Toast.makeText(
                this,
                "Usuario " + userList.get(position).getUserName() + " seleccionado.",
                Toast.LENGTH_LONG).show();
        setResult(RESULT_CODE_MANAGEMENT_USER);
        finish();
    }

    private AlertDialog dialogNewUserForm() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ManagementUsersActivity.this);

        final EditText etUserName = new EditText(ManagementUsersActivity.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        etUserName.setHint(R.string.enter_name_lastname);
        etUserName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        etUserName.setLayoutParams(lp);

        builder.setTitle(R.string.new_user)
                //.setMessage("Indique el motivo")
                .setPositiveButton(R.string.create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (etUserName.length() < 1){
                            Toast.makeText(getBaseContext(), R.string.must_enter_name, Toast.LENGTH_LONG).show();
                        }
                        else {
                            createUser(etUserName.getText().toString());
                        }
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        builder.setView(etUserName);

        return builder.create();
    }

    private void createUser(String userName) {

        User user = new User(userName);

        new CreateUser().execute(user);
    }

    private class CreateUser extends AsyncTask<User, Void, Long>{

        ProgressDialog progressDialog;
        User mUser;

        @Override
        protected Long doInBackground(User... users) {

            mUser = users[0];
            return dbHelper.createUser(users[0]);
        }

        @Override
        protected void onPreExecute() {

            progressDialog = ProgressDialog.show(ManagementUsersActivity.this, getString(R.string.saving), null, true);
        }

        @Override
        protected void onPostExecute(Long result) {

            if (result < 1){
                Toast.makeText(getBaseContext(), R.string.error_saving_user, Toast.LENGTH_LONG).show();
                if (progressDialog.isShowing())
                    progressDialog.dismiss();
                return;
            }

            if (progressDialog.isShowing())
                progressDialog.dismiss();

            reloadUserList();
        }
    }

    private void reloadUserList() {

        new LoadUsers().execute();
    }

    private class LoadUsers extends AsyncTask<Void, Void, Cursor>{

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(ManagementUsersActivity.this, getString(R.string.loading_users), null, true);
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            return dbHelper.getUsers();
        }

        @Override
        protected void onPostExecute(Cursor rows) {
            userList.clear();
            adapter = null;

            if (rows != null && rows.getCount() > 0){

                while (rows.moveToNext()){
                    User user = new User();

                    user.setId(rows.getInt(0));
                    user.setUserName(rows.getString(1));

                    userList.add(user);
                }

                adapter = new UserListAdapter(getBaseContext(), userList);
                lvUsers.setAdapter(adapter);
            }
            else {
                Toast.makeText(getBaseContext(), "No hay usuarios, crea uno nuevo.", Toast.LENGTH_LONG).show();
            }

            if (progressDialog.isShowing())
                progressDialog.dismiss();

        }
    }
}
