package com.disantek.apps.interactivebook;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

/**
 * Created by edisante@gmail.com on 10/17/2017.
 */

public class TabReading extends Fragment {

    public TabReading() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tab_reading, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        ImageButton btnReadingTest = (ImageButton) view.findViewById(R.id.ibReadingTest);
        btnReadingTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), StoryListActivity.class);
                intent.putExtra("idTestType", 1);
                startActivity(intent);
            }
        });
    }

    /*private void loadIconTest(String resName, ImageButton mImageButton) {
        int resId = getActivity().getResources().getIdentifier(resName, "drawable", getActivity().getPackageName());
        if (resId > 1){
            mImageButton.setImageResource(resId);
        }
    }*/
}
