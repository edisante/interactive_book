package com.disantek.apps.interactivebook;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.disantek.apps.interactivebook.R;
import com.woxthebox.draglistview.DragItem;
import com.woxthebox.draglistview.DragItemAdapter;

import java.util.ArrayList;
import java.util.List;

import data.StoryAnswer;

/**
 * Created by edisante@gmail.com on 28/10/2017.
 */

public class PuzzleAdapter extends DragItemAdapter<StoryAnswer, PuzzleAdapter.ViewHolder> {

    private Context context;
    private ArrayList<StoryAnswer> answerList;
    private int mLayoutId;
    private int mGrabHandleId;
    private boolean mDragOnLongPress;

    public PuzzleAdapter(Context context, ArrayList<StoryAnswer> answerList, int mLayoutId, int mGrabHandleId, boolean mDragOnLongPress) {
        this.context = context;
        this.answerList = answerList;
        this.mLayoutId = mLayoutId;
        this.mGrabHandleId = mGrabHandleId;
        this.mDragOnLongPress = mDragOnLongPress;
        setItemList(answerList);
    }

    @Override
    public long getUniqueItemId(int position) {
        return position;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(mLayoutId, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);
        String imgResource = mItemList.get(position).getAnswerIcon();
        int resId = context.getResources().getIdentifier(imgResource, "drawable", context.getPackageName());
        if (resId > 1){
            holder.ivPuzzleImg.setImageResource(resId);
        }
        holder.ivPuzzleImg.setTag(mItemList.get(position).getId());
    }

    class ViewHolder extends DragItemAdapter.ViewHolder{
        ImageView ivPuzzleImg;

        public ViewHolder(final View itemView) {
            super(itemView, mGrabHandleId, mDragOnLongPress);
            ivPuzzleImg = (ImageView) itemView.findViewById(R.id.ivPuzzleImg);
        }

        @Override
        public boolean onItemLongClicked(View view) {
            return super.onItemLongClicked(view);
        }

        @Override
        public void onItemClicked(View view) {
            super.onItemClicked(view);
        }
    }
}
