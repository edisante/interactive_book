package com.disantek.apps.interactivebook;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Toast;

import com.woxthebox.draglistview.DragListView;

import org.askerov.dynamicgrid.DynamicGridView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import data.DBHelper;
import data.StoryAnswer;
import data.StoryPuzzle;
import data.StoryPuzzlePiece;

public class StoryPuzzleActivity extends AppCompatActivity {

    private DragListView mDragListView;
    private DynamicGridView gridView;
    private Bundle evaluationResult = null;
    private int storyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_puzzle);

        gridView = (DynamicGridView) findViewById(R.id.dlvPuzzle);

        evaluationResult = getIntent().getExtras();
        storyId = getIntent().getIntExtra("storyId", -1);

        new LoadStoryPuzzle().execute(1);

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                gridView.startEditMode(i);
                return true;
            }
        });

        gridView.setOnDropListener(new DynamicGridView.OnDropListener() {
            @Override
            public void onActionDrop() {
                if (gridView.isEditMode()){
                    gridView.stopEditMode();
                }
            }
        });

        Button btnFinishPuzzle = (Button) findViewById(R.id.btnFinishQuiz);
        btnFinishPuzzle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickBtnFinishPuzzle();
            }
        });
    }

    private void onClickBtnFinishPuzzle() {

        boolean isPuzzleCorrect = true;
        StoryPuzzlePiece puzzlePiece = null;
        int order = 1;

        for (int i = 0; i < gridView.getCount(); i++) {
            puzzlePiece = (StoryPuzzlePiece) gridView.getAdapter().getItem(i);

            if (puzzlePiece.getOrder() == order){
                order++;
            }
            else {
                isPuzzleCorrect = false;
                break;
            }
        }

        Intent intent = new Intent(StoryPuzzleActivity.this, StoryEvaluationResultActivity.class);
        intent.putExtra("isPuzzleCorrect", isPuzzleCorrect);
        intent.putExtras(evaluationResult);
        startActivity(intent);

    }

    private class LoadStoryPuzzle extends AsyncTask<Integer, Void, StoryPuzzle>{

        ProgressDialog progressDialog;
        private DBHelper dbHelper = new DBHelper(getBaseContext());

        @Override
        protected void onPreExecute() {
            progressDialog = ProgressDialog.show(StoryPuzzleActivity.this, getString(R.string.loading), null, true);
        }

        @Override
        protected StoryPuzzle doInBackground(Integer... integers) {
            return dbHelper.getStoryPuzzle(integers[0]);
        }

        @Override
        protected void onPostExecute(StoryPuzzle storyPuzzle) {

            progressDialog.dismiss();
            if (storyPuzzle == null){
                Toast.makeText(getBaseContext(), R.string.error_loading_puzzle, Toast.LENGTH_LONG).show();
                return;
            }

            List<StoryPuzzlePiece> puzzlePieceList = storyPuzzle.getPuzzlePieceList();
            Collections.shuffle(puzzlePieceList);

            GridViewAdapter adapter = new GridViewAdapter(getBaseContext(), puzzlePieceList, getResources().getInteger(R.integer.column_count));
            gridView.setAdapter(adapter);
        }
    }
}
