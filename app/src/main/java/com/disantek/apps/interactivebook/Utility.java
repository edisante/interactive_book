package com.disantek.apps.interactivebook;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by edisante@gmail.com
 */

public class Utility {

    //private Context context;
    public static int NO_OPTIONS=0;

    public boolean isOnline(Context context) {
        //Context context = getApplicationContext();
        ConnectivityManager connectMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = connectMgr.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected){
            Log.i("NETWORK", "Network available");
        }
        else{
            Log.i("NETWORK", "No Network available");
        }
        /*if (connectMgr != null) {
            NetworkInfo[] netInfo = connectMgr.getAllNetworkInfo();
            if (netInfo != null) {
                for (NetworkInfo net : netInfo) {
                    Log.i("Estado Conexion", ":" + net.getState() + "Tipo: " + net.getType());
                    if (net.getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        else {
            Log.d("NETWORK", "No network available");
        }*/
        return isConnected;
    }

    public Bitmap resizeBitmap(String imagePath, int targetW, int targetH){

        if (targetW <= 0 || targetH <= 0)
            return null;

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        //bmOptions.inJustDecodeBounds = true;
        bmOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bm = BitmapFactory.decodeFile(imagePath, bmOptions);

        int width = bm.getWidth();

        int height = bm.getHeight();

        if (width > height) {
            // landscape
            float ratio = (float) width / targetW;
            width = targetW;
            height = (int)(height / ratio);
        } else if (height > width) {
            // portrait
            float ratio = (float) height / targetH;
            height = targetH;
            width = (int)(width / ratio);
        } else {
            // square
            height = targetH;
            width = targetW;
        }

        return Bitmap.createScaledBitmap(bm, width, height, true);
    }

    public void setSession(Context context, String token, int userId, String userName, String userEmail, String userPassword){

        SharedPreference.clearSession(context);

        SharedPreference.setToken(context, token);
        SharedPreference.setUserId(context, userId);
        SharedPreference.setUserName(context, userName);
        SharedPreference.setUserEmail(context, userEmail);
        SharedPreference.setUserPass(context, userPassword);
    }


    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    public boolean isLogged(Context context){
        if(SharedPreference.getUserName(context).isEmpty())
            return false;

        return true;
    }

    private static String convertToHex(byte[] data) throws IOException {
        StringBuffer sb = new StringBuffer();
        String hex=null;

        hex= Base64.encodeToString(data, 0, data.length, NO_OPTIONS);

        sb.append(hex);

        return sb.toString();
    }

    public static final String computeSHAHash(String password)
    {
        MessageDigest mdSha1 = null;
        String SHAHash = null;
        try
        {
            mdSha1 = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e1) {
            Log.e("myapp", "Error initializing SHA1 message digest");
        }
        try {
            mdSha1.update(password.getBytes("ASCII"));
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        byte[] data = mdSha1.digest();
        try {
            SHAHash = convertToHex(data);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return SHAHash;
    }

    public static final String encodeMD5(final String password) {
        try {

            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
