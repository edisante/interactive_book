package data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by edisante@gmail.com on 26/10/2017.
 */

public class StoryAnswer implements Parcelable{

    private int id;
    private int questionId;
    private String answer;
    private String answerImageResource;
    private String answerIcon;
    private int correctAnswer = 0;

    public StoryAnswer() {
    }

    public StoryAnswer(int questionId, String answer, String answerIcon) {
        this.questionId = questionId;
        this.answer = answer;
        this.answerIcon = answerIcon;
    }

    public StoryAnswer(int questionId, String answer, String answerIcon, int correctAnswer) {
        this.questionId = questionId;
        this.answer = answer;
        this.answerIcon = answerIcon;
        this.correctAnswer = correctAnswer;
    }

    public StoryAnswer(int questionId, String answer) {
        this.questionId = questionId;
        this.answer = answer;
    }

    public StoryAnswer(int id, int questionId, String answer, String answerImageResource, String answerIcon) {
        this.id = id;
        this.questionId = questionId;
        this.answer = answer;
        this.answerImageResource = answerImageResource;
        this.answerIcon = answerIcon;
    }

    public StoryAnswer(int id, int questionId, String answer, String answerIcon, int correctAnswer) {
        this.id = id;
        this.questionId = questionId;
        this.answer = answer;
        this.answerIcon = answerIcon;
        this.correctAnswer = correctAnswer;
    }

    protected StoryAnswer(Parcel in) {
        id = in.readInt();
        questionId = in.readInt();
        answer = in.readString();
        answerImageResource = in.readString();
        answerIcon = in.readString();
        correctAnswer = in.readInt();
    }

    public static final Creator<StoryAnswer> CREATOR = new Creator<StoryAnswer>() {
        @Override
        public StoryAnswer createFromParcel(Parcel in) {
            return new StoryAnswer(in);
        }

        @Override
        public StoryAnswer[] newArray(int size) {
            return new StoryAnswer[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnswerImageResource() {
        return answerImageResource;
    }

    public void setAnswerImageResource(String answerImageResource) {
        this.answerImageResource = answerImageResource;
    }

    public String getAnswerIcon() {
        return answerIcon;
    }

    public void setAnswerIcon(String answerIcon) {
        this.answerIcon = answerIcon;
    }

    public int isCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(int correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(questionId);
        parcel.writeString(answer);
        parcel.writeString(answerImageResource);
        parcel.writeString(answerIcon);
        parcel.writeInt(correctAnswer);
    }
}
