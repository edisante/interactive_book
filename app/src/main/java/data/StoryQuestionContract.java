package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 26/10/2017.
 */

public class StoryQuestionContract {

    public static abstract class StoryQuestionEntry implements BaseColumns {

        public static final String TABLE_NAME = "story_questions";

        public static final String _ID = "id";
        public static final String STORY_ID = "story_id";
        public static final String QUESTION = "question";
        public static final String QUESTION_IMAGE_RESOURCE = "question_image_resource";
        public static final String QUESTION_LAYOUT_RESOURCE = "question_layout_resource";
    }
}
