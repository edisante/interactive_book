package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 29/10/2017.
 */

public class StoryPuzzlePieceContract {

    public static abstract class StoryPuzzlePieceEntry implements BaseColumns {

        public static final String TABLE_NAME = "story_puzzle_pieces";

        public static final String _ID = "id";
        public static final String STORY_PUZZLE_ID = "story_puzzle_id";
        public static final String IMG_RESOURCE = "img_resource";
        public static final String ORDER = "piece_order";
    }
}
