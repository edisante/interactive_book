package data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edisante@gmail.com on 29/10/2017.
 */

public class StoryPuzzle {

    private int id;
    private int storyId;
    private String description;

    private List<StoryPuzzlePiece> puzzlePieceList = new ArrayList<>();

    public StoryPuzzle() {
    }

    public StoryPuzzle(int storyId) {
        this.storyId = storyId;
    }

    public StoryPuzzle(int id, int storyId, String description) {
        this.id = id;
        this.storyId = storyId;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<StoryPuzzlePiece> getPuzzlePieceList() {
        return puzzlePieceList;
    }

    public void setPuzzlePieceList(List<StoryPuzzlePiece> puzzlePieceList) {
        this.puzzlePieceList = puzzlePieceList;
    }
}
