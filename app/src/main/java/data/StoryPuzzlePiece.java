package data;

/**
 * Created by edisante@gmail.com on 29/10/2017.
 */

public class StoryPuzzlePiece {

    private int id;
    private int storyPuzzleId;
    private String imgResource;
    private int order;

    public StoryPuzzlePiece() {
    }

    public StoryPuzzlePiece(int storyPuzzleId, String imgResource, int order) {
        this.storyPuzzleId = storyPuzzleId;
        this.imgResource = imgResource;
        this.order = order;
    }

    public StoryPuzzlePiece(int id, int storyPuzzleId, String imgResource, int order) {
        this.id = id;
        this.storyPuzzleId = storyPuzzleId;
        this.imgResource = imgResource;
        this.order = order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoryPuzzleId() {
        return storyPuzzleId;
    }

    public void setStoryPuzzleId(int storyPuzzleId) {
        this.storyPuzzleId = storyPuzzleId;
    }

    public String getImgResource() {
        return imgResource;
    }

    public void setImgResource(String imgResource) {
        this.imgResource = imgResource;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
