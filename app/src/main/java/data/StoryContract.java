package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 10/18/2017.
 */

public class StoryContract {

    public static abstract class StoryEntry implements BaseColumns {

        public static final String TABLE_NAME = "stories";

        public static final String _ID = "id";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String AUTHOR = "author";
        public static final String ICON_RESOURCE = "iconResource";
        public static final String ACTIVE = "active";
    }
}
