package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 29/10/2017.
 */

public class StoryPuzzleContract {

    public static abstract class StoryPuzzleEntry implements BaseColumns {

        public static final String TABLE_NAME = "story_puzzles";

        public static final String _ID = "id";
        public static final String STORY_ID = "story_id";
        public static final String DESCRIPTION = "description";
    }
}
