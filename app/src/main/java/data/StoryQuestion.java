package data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by edisante@gmail.com on 26/10/2017.
 */

public class StoryQuestion {

    private int id;
    private int StoryId;
    private String question;
    private String questionImageResource;
    private String questionLayoutResource;

    private List<StoryAnswer> answerList = new ArrayList<>();

    public StoryQuestion() {
    }

    public StoryQuestion(int storyId, String question) {
        StoryId = storyId;
        this.question = question;
    }

    public StoryQuestion(int storyId, String question, List<StoryAnswer> answerList) {
        StoryId = storyId;
        this.question = question;
        this.answerList = answerList;
    }

    public StoryQuestion(int id, int storyId, String question, String questionLayoutResource) {
        this.id = id;
        StoryId = storyId;
        this.question = question;
        this.questionLayoutResource = questionLayoutResource;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStoryId() {
        return StoryId;
    }

    public void setStoryId(int storyId) {
        StoryId = storyId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionImageResource() {
        return questionImageResource;
    }

    public void setQuestionImageResource(String questionImageResource) {
        this.questionImageResource = questionImageResource;
    }

    public List<StoryAnswer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<StoryAnswer> answerList) {
        this.answerList = answerList;
    }

    public String getQuestionLayoutResource() {
        return questionLayoutResource;
    }

    public void setQuestionLayoutResource(String questionLayoutResource) {
        this.questionLayoutResource = questionLayoutResource;
    }
}
