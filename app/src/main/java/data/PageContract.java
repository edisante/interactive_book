package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 10/20/2017.
 */

public class PageContract {

    public static abstract class PageEntry implements BaseColumns {

        public static final String TABLE_NAME = "pages";

        public static final String _ID = "id";
        public static final String RESOURCE_NAME = "resourceName";
        public static final String PAGE_TYPE = "pageType";
        public static final String STORY_ID = "storyId";
    }
}
