package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 26/10/2017.
 */

public class StoryAnswerContract {

    public static abstract class StoryAnswerEntry implements BaseColumns {

        public static final String TABLE_NAME = "story_answers";

        public static final String _ID = "id";
        public static final String QUESTION_ID = "question_id";
        public static final String ANSWER = "answer";
        public static final String ANSWER_IMAGE_RESOURCE = "answer_image_resource";
        public static final String ANSWER_ICON = "answer_icon";
        public static final String IS_CORRECT_ANSWER = "is_correct_answer";
    }
}
