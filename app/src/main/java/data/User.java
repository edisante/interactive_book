package data;

/**
 * Created by edisante@gmail.com on 10/16/2017.
 */

public class User {

    private int id;
    private String userName;
    private String firstName;
    private String lastName;
    private boolean active;

    public User(int id, String userName, String firstName, String lastName, boolean active) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.active = active;
    }

    public User(String userName) {
        this.userName = userName;
    }

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
