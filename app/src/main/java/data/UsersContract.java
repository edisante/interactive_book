package data;

import android.provider.BaseColumns;

/**
 * Created by edisante@gmail.com on 10/16/2017.
 */

public class UsersContract {

    public static abstract class UserEntry implements BaseColumns {

        public static final String TABLE_NAME = "users";

        public static final String _ID = "id";
        public static final String USER_NAME = "userName";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String ACTIVE = "active";
    }
}
