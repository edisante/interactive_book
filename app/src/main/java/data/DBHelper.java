package data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static data.PageContract.*;
import static data.StoryAnswerContract.*;
import static data.StoryContract.*;
import static data.StoryPuzzleContract.*;
import static data.StoryPuzzlePieceContract.*;
import static data.StoryQuestionContract.*;
import static data.UsersContract.*;

/**
 * Created by edisante@gmail.com on 10/16/2017.
 */

public class DBHelper extends SQLiteOpenHelper{

    public static int DB_VERSION = 5;
    public static final String DB_NAME = "interactive_book.db";
    private Context mContext;

    private static final String CREATE_TABLE_STORIES =
            "CREATE TABLE " + StoryEntry.TABLE_NAME + " (" +
                    StoryEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    StoryEntry.TITLE + " TEXT NOT NULL," +
                    StoryEntry.DESCRIPTION + " TEXT," +
                    StoryEntry.AUTHOR + " TEXT," +
                    StoryEntry.ICON_RESOURCE + " TEXT," +
                    StoryEntry.ACTIVE + " INTEGER DEFAULT 1," +
                    "UNIQUE (" + StoryEntry.TITLE + "));";

    private static final String CREATE_TABLE_PAGES =
            "CREATE TABLE " + PageEntry.TABLE_NAME + " (" +
                    PageEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    PageEntry.RESOURCE_NAME + " TEXT NOT NULL," +
                    PageEntry.PAGE_TYPE + " INTEGER DEFAULT 1," +
                    PageEntry.STORY_ID + " INTEGER NOT NULL," +
                    " FOREIGN KEY (" + PageEntry.STORY_ID + ") REFERENCES " +
                    StoryEntry.TABLE_NAME + "(" + StoryEntry._ID + "));";

    private static final String CREATE_TABLE_STORY_QUESTION =
            "CREATE TABLE " + StoryQuestionEntry.TABLE_NAME + " (" +
                    StoryQuestionEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    StoryQuestionEntry.STORY_ID + " INTEGER NOT NULL," +
                    StoryQuestionEntry.QUESTION + " TEXT NOT NULL," +
                    StoryQuestionEntry.QUESTION_IMAGE_RESOURCE + " TEXT," +
                    StoryQuestionEntry.QUESTION_LAYOUT_RESOURCE + " TEXT," +
                    " FOREIGN KEY (" + StoryQuestionEntry.STORY_ID + ") REFERENCES " +
                    StoryEntry.TABLE_NAME + "(" + StoryEntry._ID + "));";

    private static final String CREATE_TABLE_STORY_ANSWER =
            "CREATE TABLE " + StoryAnswerEntry.TABLE_NAME + " (" +
                    StoryAnswerEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    StoryAnswerEntry.QUESTION_ID + " INTEGER NOT NULL," +
                    StoryAnswerEntry.ANSWER + " TEXT NOT NULL," +
                    StoryAnswerEntry.ANSWER_ICON + " TEXT," +
                    StoryAnswerEntry.IS_CORRECT_ANSWER + " INTEGER DEFAULT 0," +
                    " FOREIGN KEY (" + StoryAnswerEntry.QUESTION_ID + ") REFERENCES " +
                    StoryQuestionEntry.TABLE_NAME + "(" + StoryQuestionEntry._ID + "));";

    private static final String CREATE_TABLE_STORY_PUZZLES =
            "CREATE TABLE " + StoryPuzzleEntry.TABLE_NAME + " (" +
                    StoryPuzzleEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    StoryPuzzleEntry.STORY_ID + " INTEGER NOT NULL," +
                    StoryPuzzleEntry.DESCRIPTION + " TEXT," +
                    " FOREIGN KEY (" + StoryPuzzleEntry.STORY_ID + ") REFERENCES " +
                    StoryEntry.TABLE_NAME + "(" + StoryEntry._ID + "));";

    private static final String CREATE_TABLE_STORY_PUZZLE_PIECES =
            "CREATE TABLE " + StoryPuzzlePieceEntry.TABLE_NAME + " (" +
                    StoryPuzzlePieceEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    StoryPuzzlePieceEntry.STORY_PUZZLE_ID + " INTEGER NOT NULL," +
                    StoryPuzzlePieceEntry.IMG_RESOURCE + " TEXT NOT NULL," +
                    StoryPuzzlePieceEntry.ORDER + " INTEGER NOT NULL," +
                    " FOREIGN KEY (" + StoryPuzzlePieceEntry.STORY_PUZZLE_ID + ") REFERENCES " +
                    StoryPuzzleEntry.TABLE_NAME + "(" + StoryPuzzleEntry._ID + "));";

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        final String SQL_CREATE_TABLES =
                "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                        UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        UserEntry.USER_NAME + " TEXT NOT NULL," +
                        UserEntry.FIRST_NAME + " TEXT," +
                        UserEntry.LAST_NAME + " TEXT," +
                        UserEntry.ACTIVE + " INTEGER DEFAULT 1," +
                        "UNIQUE (" + UserEntry.USER_NAME + "));";

        sqLiteDatabase.execSQL(SQL_CREATE_TABLES);
        onUpgrade(sqLiteDatabase, 1, DB_VERSION);
    }

    /**
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Updating DB from version " + oldVersion + " to " + newVersion);

        if (oldVersion < 2){
            //Log.i(TAG, "onUpgrade: Running update version: old:"+oldVersion+", new:"+newVersion);
            db.execSQL(CREATE_TABLE_STORIES);
            Story story = new Story("El Dibujo Parlante", "story_1_icon");
            createStory(story, db);
        }

        if (oldVersion < 3){
            db.execSQL(CREATE_TABLE_PAGES);
            List<Page> pages = new ArrayList<>();

            Page page = new Page("story_1_text_1", 1);
            pages.add(page);
            page = new Page("story_1_text_2", 1);
            pages.add(page);
            page = new Page("story_1_text_3", 1);
            pages.add(page);
            page = new Page("story_1_text_4", 1);
            pages.add(page);
            page = new Page("story_1_text_5", 1);
            pages.add(page);
            page = new Page("story_1_text_6", 1);
            pages.add(page);
            page = new Page("story_1_text_7", 1);
            pages.add(page);
            page = new Page("story_1_text_8", 1);
            pages.add(page);

            createStoryPages(pages, db);
        }

        if (oldVersion < 4){
            db.execSQL(CREATE_TABLE_STORY_QUESTION);
            db.execSQL(CREATE_TABLE_STORY_ANSWER);

            // Question 1
            StoryQuestion question = new StoryQuestion(1, 1, "¿Quién motivó a Tintín a SEGUIR pintando?", "layout_question_1");
            long questionId = createStoryQuestion(question, db);

            if (questionId < 1){
                Log.i(TAG, "onUpgrade: Error creating question 1");
                return;
            }

            List<StoryAnswer> answerList = new ArrayList<>();
            answerList.add(new StoryAnswer((int)questionId, "El Lápiz", "answer_1_question_1", 0));
            answerList.add(new StoryAnswer((int)questionId, "El Papel", "answer_2_question_1", 0));
            answerList.add(new StoryAnswer((int)questionId, "El Dibujo", "answer_3_question_1", 1));
            answerList.add(new StoryAnswer((int)questionId, "Los Ojos", "answer_4_question_1", 0));

            createStoryQuestionAnswer(answerList, db);

            //Question 2
            question = new StoryQuestion(2, 1, "¿Con que objeto cambiaron los dibujos Tintín?", "layout_question_2");
            questionId = createStoryQuestion(question, db);

            if (questionId < 1){
                Log.i(TAG, "onUpgrade: Error creating question 2");
                return;
            }

            answerList.clear();
            answerList.add(new StoryAnswer((int)questionId, "Un Libro", "answer_1_question_2", 0));
            answerList.add(new StoryAnswer((int)questionId, "Una Silla", "answer_2_question_2", 0));
            answerList.add(new StoryAnswer((int)questionId, "Un Lápiz", "answer_3_question_2", 1));
            answerList.add(new StoryAnswer((int)questionId, "Un Círculo", "answer_4_question_2", 0));

            createStoryQuestionAnswer(answerList, db);

            //Question 3
            question = new StoryQuestion(3, 1, "¿Cuál es la Palabra que falta?\n" +
                    "Tintín Comprendió que solo intentando y Practicando una y otra vez podría lograr realizar el dibujo que él quisiera, pero al mismo tiempo nunca debía dejar de...", "layout_question_3");
            questionId = createStoryQuestion(question, db);

            if (questionId < 1){
                Log.i(TAG, "onUpgrade: Error creating question 3");
                return;
            }

            answerList.clear();
            answerList.add(new StoryAnswer((int)questionId, "a) Lograr", "answer_1_question_3", 0));
            answerList.add(new StoryAnswer((int)questionId, "b) Llorar", "answer_2_question_3", 0));
            answerList.add(new StoryAnswer((int)questionId, "c) Disgustarse", "answer_3_question_3", 0));
            answerList.add(new StoryAnswer((int)questionId, "d) Divertirse", "answer_4_question_3", 1));

            createStoryQuestionAnswer(answerList, db);

        }

        if (oldVersion < 5){
            db.execSQL(CREATE_TABLE_STORY_PUZZLES);
            db.execSQL(CREATE_TABLE_STORY_PUZZLE_PIECES);

            long puzzleId = createStoryPuzzle(
                    new StoryPuzzle(1, 1, null),
                    db
            );

            for (int i = 1; i <= 9; i++) {
                createStoryPuzzlePiece(
                        new StoryPuzzlePiece((int)puzzleId, "puzzle_img_" + i, i),
                        db
                );
            }
        }
    }

    private long createStoryPuzzlePiece(StoryPuzzlePiece storyPuzzlePiece, SQLiteDatabase db) {

        if (storyPuzzlePiece == null){
            Log.e(getClass().getName(), "createStoryPuzzlePiece: storyPuzzlePiece is null");
            return -1;
        }

        ContentValues values = new ContentValues();
        values.put(StoryPuzzlePieceEntry.STORY_PUZZLE_ID, storyPuzzlePiece.getStoryPuzzleId());
        values.put(StoryPuzzlePieceEntry.IMG_RESOURCE, storyPuzzlePiece.getImgResource());
        values.put(StoryPuzzlePieceEntry.ORDER, storyPuzzlePiece.getOrder());

        return db.insert(
                StoryPuzzlePieceEntry.TABLE_NAME,
                null,
                values
        );
    }

    private long createStoryPuzzle(StoryPuzzle storyPuzzle, SQLiteDatabase db) {

        if (storyPuzzle == null){
            Log.e(getClass().getName(), "createStoryPuzzle: storyPuzzle is null");
            return -1;
        }

        ContentValues contentValues = new ContentValues();

        contentValues.put(StoryPuzzleEntry._ID, storyPuzzle.getId());
        contentValues.put(StoryPuzzleEntry.STORY_ID, storyPuzzle.getStoryId());

        return db.insert(
                StoryPuzzleEntry.TABLE_NAME,
                null,
                contentValues
        );
    }

    private boolean createStoryQuestionAnswer(List<StoryAnswer> answerList, SQLiteDatabase db) {

        if (answerList == null || answerList.size() < 1){
            Log.i(TAG, "createStoryQuestionAnswer: error creating answers");
            return false;
        }

        for (int i = 0; i < answerList.size(); i++) {

            ContentValues values = new ContentValues();

            values.put(StoryAnswerEntry.QUESTION_ID, answerList.get(i).getQuestionId());
            values.put(StoryAnswerEntry.ANSWER, answerList.get(i).getAnswer());
            values.put(StoryAnswerEntry.ANSWER_ICON, answerList.get(i).getAnswerIcon());
            values.put(StoryAnswerEntry.IS_CORRECT_ANSWER, answerList.get(i).isCorrectAnswer());

            db.insert(
                    StoryAnswerEntry.TABLE_NAME,
                    null,
                    values
            );
        }

        return true;
    }

    private long createStoryQuestion(StoryQuestion question, SQLiteDatabase db) {

        ContentValues values = new ContentValues();

        if (question == null){
            Log.i(TAG, "createStoryQuestion: question is null");
            return -1;
        }

        values.put(StoryQuestionEntry._ID, question.getId());
        values.put(StoryQuestionEntry.STORY_ID, question.getStoryId());
        values.put(StoryQuestionEntry.QUESTION, question.getQuestion());
        values.put(StoryQuestionEntry.QUESTION_LAYOUT_RESOURCE, question.getQuestionLayoutResource());

        return db.insert(
                StoryQuestionEntry.TABLE_NAME,
                null,
                values
        );
    }

    public StoryPuzzle getStoryPuzzle(int puzzleId){

        StoryPuzzle storyPuzzle = null;

        if (puzzleId < 1){
            Log.e(getClass().getName(), "getStoryPuzzle: puzzleId is null");
            return null;
        }

        Cursor cursorPuzzle = getReadableDatabase().query(
                StoryPuzzleEntry.TABLE_NAME,
                null,
                StoryPuzzleEntry._ID + " = ? ",
                new String[] {puzzleId + ""},
                null,
                null,
                null
        );

        if (cursorPuzzle != null && cursorPuzzle.getCount() > 0){

            if (cursorPuzzle.moveToFirst()){

                storyPuzzle = new StoryPuzzle(
                        cursorPuzzle.getInt(cursorPuzzle.getColumnIndex(StoryPuzzleEntry._ID)),
                        cursorPuzzle.getInt(cursorPuzzle.getColumnIndex(StoryPuzzleEntry.STORY_ID)),
                        cursorPuzzle.getString(cursorPuzzle.getColumnIndex(StoryPuzzleEntry.DESCRIPTION))
                );

                Cursor cursorPuzzlePieces = getReadableDatabase().query(
                        StoryPuzzlePieceEntry.TABLE_NAME,
                        null,
                        StoryPuzzlePieceEntry.STORY_PUZZLE_ID + " = ? ",
                        new String[] {storyPuzzle.getId() + ""},
                        null,
                        null,
                        null
                );

                List<StoryPuzzlePiece> puzzlePieceList = new ArrayList<>();

                while (cursorPuzzlePieces.moveToNext()){

                    puzzlePieceList.add(new StoryPuzzlePiece(
                            cursorPuzzlePieces.getInt(cursorPuzzlePieces.getColumnIndex(StoryPuzzlePieceEntry._ID)),
                            cursorPuzzlePieces.getInt(cursorPuzzlePieces.getColumnIndex(StoryPuzzlePieceEntry.STORY_PUZZLE_ID)),
                            cursorPuzzlePieces.getString(cursorPuzzlePieces.getColumnIndex(StoryPuzzlePieceEntry.IMG_RESOURCE)),
                            cursorPuzzlePieces.getInt(cursorPuzzlePieces.getColumnIndex(StoryPuzzlePieceEntry.ORDER))
                    ));
                }

                storyPuzzle.setPuzzlePieceList(puzzlePieceList);
            }
            return storyPuzzle;
        }
        else {
            Log.w(getClass().getName(), "getStoryPuzzle: No Results");
            return null;
        }

    }

    public List<StoryQuestion> getQuestionsByStory(int storyId){

        List<StoryQuestion> questionList = new ArrayList<>();

        if (storyId < 1){
            Log.e(mContext.getClass().getName(), "getQuestionsByStory: ");
            return null;
        }

        Cursor cursor = getReadableDatabase().query(
                StoryQuestionEntry.TABLE_NAME,
                null,
                StoryQuestionEntry.STORY_ID + " = " + storyId,
                null,
                null,
                null,
                null
        );

        if (cursor != null && cursor.getCount() > 0){

            while (cursor.moveToNext()){

                //Busco las respuestas para cada pregunta
                Cursor cursorAnswers = getReadableDatabase().query(
                        StoryAnswerEntry.TABLE_NAME,
                        null,
                        StoryAnswerEntry.QUESTION_ID + " = " + cursor.getInt(cursor.getColumnIndex(StoryQuestionEntry._ID)),
                        null,
                        null,
                        null,
                        null
                );

                StoryQuestion question = new StoryQuestion(
                        cursor.getInt(cursor.getColumnIndex(StoryQuestionEntry._ID)),
                        cursor.getInt(cursor.getColumnIndex(StoryQuestionEntry.STORY_ID)),
                        cursor.getString(cursor.getColumnIndex(StoryQuestionEntry.QUESTION)),
                        cursor.getString(cursor.getColumnIndex(StoryQuestionEntry.QUESTION_LAYOUT_RESOURCE)));

                if (cursorAnswers != null && cursorAnswers.getCount() > 0){
                    List<StoryAnswer> answerList = new ArrayList<>();
                    while (cursorAnswers.moveToNext()){
                        answerList.add(new StoryAnswer(
                                cursorAnswers.getInt(cursorAnswers.getColumnIndex(StoryAnswerEntry._ID)),
                                cursorAnswers.getInt(cursorAnswers.getColumnIndex(StoryAnswerEntry.QUESTION_ID)),
                                cursorAnswers.getString(cursorAnswers.getColumnIndex(StoryAnswerEntry.ANSWER)),
                                cursorAnswers.getString(cursorAnswers.getColumnIndex(StoryAnswerEntry.ANSWER_ICON)),
                                cursorAnswers.getInt(cursorAnswers.getColumnIndex(StoryAnswerEntry.IS_CORRECT_ANSWER))
                        ));
                    }

                    question.setAnswerList(answerList);
                }

                questionList.add(question);
            }
        }
        else {
            Log.w(mContext.getClass().getName(), "getQuestionsByStory: No results");
            return null;
        }

        return questionList;
    }

    public List<Page> getStoryPageList(int storyId){

        List<Page> pages = new ArrayList<>();

        if (storyId < 1){
            return null;
        }

        Cursor cursor = getReadableDatabase().query(
                PageEntry.TABLE_NAME,
                null,
                PageEntry.STORY_ID + " = " + storyId,
                null,
                null,
                null,
                null
        );

        if (cursor != null && cursor.getCount() > 0){

            while (cursor.moveToNext()){
                pages.add(new Page(
                        cursor.getString(cursor.getColumnIndex(PageEntry.RESOURCE_NAME)),
                        cursor.getInt(cursor.getColumnIndex(PageEntry.STORY_ID))
                ));
            }
        }
        else {
            Log.i(TAG, "getStoryPageList: No results");
            return null;
        }

        return pages;
    }

    private boolean createStoryPages(List<Page> pages, SQLiteDatabase db) {

        if (pages == null || pages.size() < 1){
            Log.i(TAG, "createStoryPages: error creating pages");
            return false;
        }

        for (int i = 0; i < pages.size(); i++) {

            ContentValues values = new ContentValues();

            values.put(PageEntry.RESOURCE_NAME, pages.get(i).getResourceName());
            values.put(PageEntry.PAGE_TYPE, pages.get(i).getPageType());
            values.put(PageEntry.STORY_ID, pages.get(i).getStoryId());

            db.insert(
                    PageEntry.TABLE_NAME,
                    null,
                    values
            );
        }

        return true;
    }

    public long createStory(Story story, SQLiteDatabase db){

        ContentValues values = new ContentValues();

        if (story == null){
            Log.i(TAG, "createStory: story is null");
            return -1;
        }

        values.put(StoryEntry.TITLE, story.getTitle());
        values.put(StoryEntry.ICON_RESOURCE, story.getIconResource());
        values.put(StoryEntry.ACTIVE, 1);

        return db.insert(
                StoryEntry.TABLE_NAME,
                null,
                values
        );
    }

    public long createUser(User user){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues values = new ContentValues();

        if (user == null){
            Log.i(TAG, "createUser: user is null");
            return -1;
        }

        values.put(UserEntry.USER_NAME, user.getUserName());
        values.put(UserEntry.ACTIVE, 1);

        return sqLiteDatabase.insert(
                UserEntry.TABLE_NAME,
                null,
                values
        );
    }

    public Cursor getUsers(){

        return getReadableDatabase().query(
                UserEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }

    public List<Story> getStories(){
        List<Story> stories = new ArrayList<>();

        Cursor cursor = getReadableDatabase().query(
                StoryEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        if (cursor != null && cursor.getCount() > 0){

            while (cursor.moveToNext()){
                Story story = new Story();

                story.setId(cursor.getInt(0));
                story.setTitle(cursor.getString(1));
                story.setIconResource(cursor.getString(4));

                stories.add(story);
            }
        }
        else {
            return null;
        }

        return stories;
    }
}
