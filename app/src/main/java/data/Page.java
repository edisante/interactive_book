package data;

/**
 * Created by edisante@gmail.com on 10/20/2017.
 */

public class Page {

    private int id;
    private String resourceName;
    private int pageType;   //1=Image, 2=Text
    private int storyId;

    public Page() {
    }

    public Page(String resourceName, int storyId) {
        this.resourceName = resourceName;
        this.storyId = storyId;
        this.pageType = 1;
    }

    public Page(String resourceName, int pageType, int storyId) {
        this.resourceName = resourceName;
        this.pageType = pageType;
        this.storyId = storyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

    public int getPageType() {
        return pageType;
    }

    public void setPageType(int pageType) {
        this.pageType = pageType;
    }

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }
}
